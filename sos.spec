%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:          sos
Version:       4.8.0
Release:       1
Summary:       A set of tools to gather troubleshooting information from a system
License:       GPLv2+
URL:           https://github.com/sosreport/sos
Source0:       https://github.com/sosreport/sos/archive/%{version}.tar.gz

Patch9000:     openEuler-add-openEuler-policy.patch
Patch9001:     add-uniontech-os-support.patch
Patch9002:     Fix-sos-command-failed-in-sos-4.0.patch
Patch9003:     add-KylinSec-OS-support.patch
Patch9004:     add-Kylin-OS-support.patch

BuildRequires: python3-devel gettext
Requires:      bzip2 xz python3-rpm tar python3-pexpect
Requires:      python3-magic
Recommends:    python3-pyyaml
BuildArch:     noarch

%description
Sos is an extensible, portable, support data collection tool primarily
aimed at Linux distributions and other UNIX-like operating systems.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%py3_build

%install
%py3_install '--install-scripts=%{_sbindir}'

install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}
install -d -m 700 %{buildroot}%{_sysconfdir}/%{name}/cleaner
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/presets.d
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/groups.d
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/extras.d
install -m 644 %{name}.conf %{buildroot}%{_sysconfdir}/%{name}/%{name}.conf

rm -rf ${RPM_BUILD_ROOT}/usr/config/

%find_lang %{name} || echo 0

# internationalization is currently broken. Uncomment this line once fixed.
#%%files -f %%{name}.lang
%files
%license LICENSE
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/sos/sos.conf
%{_sbindir}/sos*
%dir /etc/sos/cleaner
%dir /etc/sos/presets.d
%dir /etc/sos/extras.d
%dir /etc/sos/groups.d
%{python3_sitelib}/*
%exclude %{_datadir}/doc/sos/{AUTHORS,README.md}

%files help
%defattr(-,root,root)
%doc AUTHORS README.md
%{_mandir}/man1/*
%{_mandir}/man5/*

%changelog
* Tue Oct 08 2024 GuoCe <guoce@kylinos.cn> - 4.8.0-1
- update to 4.8.0
- New plugins: fail2ban, microcloud.
- The networking plugin will now capture nmstatectl output.
- Added a new policy for CloudLinux installations.

* Thu Jul 11 2024 warlcok <hunan@kylinos.cn> - 4.7.2-1
- update to 4.7.2

* Mon Nov 06 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 4.5.6-2
- add Kylin policy

* Thu Jul 27 2023 dillon chen < dillon.chen@gmail.com> - 4.5.6-1
- update to 4.5.6

* Sun Oct  9 2022 dillon chen < dillon.chen@gmail.com> - 4.4-1
- update to 4.4

* Wed Feb 23 2022 liuxingxiang <liuxingxiang@kylinsec.com.cn> - 4.0-6
- add KylinSec policy

* Tue Feb 22 2022 weidong <weidong@uniontech.com> - 4.0-5
- Fix sos command failed in sos 4.0

* Mon Jul 19 2021 liugang <liuganga@uniontech.com> - 4.0-4
- add UnionTech policy

* Mon Mar 08 2021 shixuantong <shixuantong@huawei.com> - 4.0-3
- add openEuler policy

* Wed Mar 03 2021 shixuantong <shixuantong@huawei.com> - 4.0-2
- fix unable to read configure file /etc/sos/sos.conf issue

* Tue Feb 02 2021 shixuantong <shixuantong@huawei.com> - 4.0-1
- Upgrade to version 4.0

* Mon Jul 13 2020 Jeffery.Gao <gaojianxing@huawei.com> - 3.9.1-3
- Append openEuler policy

* Mon Jun 1 2020 chengzihan <chengzihan2@huawei.com> - 3.9.1-2
- Package update

* Mon Feb 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.6-5
- Package init
